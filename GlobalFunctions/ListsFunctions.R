# Set of specific functions to work on lists
# par Valerie PEREZ
# Created on the 16/08/2022

# Sub_list_data() --------------------------------------------------------------
## List all files within a directory (set with the variable Path), while a 
# pattern to keep while avoiding a subgroup containing said pattern
Sub_list_data <- 
  function(Path = "./", # set the directory where are located the files to sort
           Match="",  # set the pattern common to all the files to keep 
           PatternToIgnore = ""){ # set the pattern common to the files to remove within the list of files to keep
    
  ListFiles <- list.files(Path, pattern=Match, full.names = FALSE) # list files that match the pattern to keep
  AllData <- list() # create a new list to store the subset list 
  n=0 # create variable to follow the loop iterations
  
  for(l in ListFiles){ # for each file in the list of files
    if(grepl(PatternToIgnore, l)==TRUE){ # identify the files with the pattern to remove
      next #ignore the file with the pattern
    }
    else{
      n=n+1 # follow iterations
      AllData[n] <- l # add file to keep in the list
    }
  }
  
  message(paste0("There are ", n, " data file(s) kept matching the pattern.")) 
  # feedback, to evaluate the number of files that passed the filter
  
  return(AllData) # return the subset list
}

################################################################################
