### Valerie_Perez_Variables_Customized_Bar Plots_Script_Analyses_COMISTYL ####
### based on the tutorial: 
#Riffomonas Project [https://riffomonas.org/code_club/], Pat Schloss

################################################################################
#Color palette
Pal_G_Col <- c("#b4adad5f", "#6d001a", "#be0039", "#ff4500",
            "#ffa800", "#fff8b8", "#00cc78", "#00ccc0",
            "#2450a4", "#94b3ff", "#811e9f", "#b44ac0",
            "#e4abff", "#ffb470", "#515252", "#ff3881", 
            "#ff99aa", "#51e9f4", "#00756f", "#6d482f", 
            "#898d90", "#9c6926", "#ffffff", "#d4d7d9")

# show_col(Pal_G_Col, cex_label = 0.9, ncol = 6)
NC_water_Col <- c("#b4adad5f","#8e8870ff","#a96017ff","#848484ff",
                  "#66ffffff", "#2450a4","#7f8eb4ff","#23e4adff",
                  "#be0039ff","#ff0066ff", "#4ad6c7ff","#b8ddcfff",
                  "#94c0d2cb","#624210ff","#12b6a4ff", "#205266ff",
                  "#09755fff","#0a1ec6ff","#a7c9d7ff","#00cea3ff")
# show_col(NC_water_Col, cex_label = 0.9, ncol = 4)

FP_water_Col <- c("#b4adad5f", "#8e8870ff", "#a96017ff", "#be0039ff",
                  "#12b6a4ff", "#2450a4", "#66ffffff", "#a7c9d7ff",
                  "#848484ff", "#ff0066ff", "#4ad6c7ff", "#205266ff",
                  "#0a1ec6ff", "#f0c9baff", "#fff8b8", "#8c23ffff",
                  "#624210ff", "#d2b6b7ff", "#d57dfdff", "#ceccffff")
# show_col(FP_water_Col, cex_label = 0.9, ncol = 4)

NC_Hem_Col <- c("#b4adad5f", "#2450a4", "#ffa800", "#fff8b8",
                "#a96017ff", "#ff4300ff", "#2ab26cff", "#be0039ff", 
                "#a2ff00ff", "#8e8870ff", "#624210ff", "#f0c9baff",
                "#18de1dff", "#abffc7ff", "#ffc4aeff", "#ffcf01ff",
                "#ff0066ff", "#1e6136ff", "#00ff8aff", "#d2b6b7ff")
# show_col(NC_Hem_Col, cex_label = 0.9, ncol = 4)

FP_Hem_Col <- c("#b4adad5f", "#2450a4", "#66ffffff", "#a96017ff", 
                "#ffa800", "#be0039ff", "#ffc4aeff", "#d57dfdff",
                "#8e8870ff", "#fff8b8", "#624210ff", "#ff4300ff", 
                "#ffcf01ff","#8d25b8ff","#848484ff", "#e3ccffff",
                "#ab96b4ff", "#f8afe7ff", "#ff33ccff", "#7e336dff")
# show_col(FP_Hem_Col, cex_label = 0.9, ncol = 4)

################################################################################
# Theme
BarPlotTheme <-  theme_classic() +
  
  theme(axis.line.x = element_line(linewidth = 1),    #size x axis
        axis.line.y = element_line(linewidth = 1),    #size y axis
        axis.text.x = element_text(color = "black",
                                   face="bold",  #x axis text (here, SampleID)  
                                   size = 15,    
                                   angle = 90,      #text rotation 
                                   vjust = 0.45),   #adjust text position
        axis.text.y = element_text(color = "black",
                                   face = "bold",   #y axis text (here pourcents) 
                                   size = 15),      
        axis.ticks = element_line(linewidth = 1),       #all ticks parameters
        axis.title = element_text(face="bold",      #adjust size y title (here, Relative Abundance (%))
                                  size = 15),   
        legend.key.size = unit(9, "pt"),           #legend colors size height (default and minimum, text height)
        legend.key.width = unit(8, "pt"),          #legend colors size width
        legend.position = "bottom",                  #legend position
        legend.text = element_markdown(size = 15),  #legend read text as markdown
        legend.title = element_blank(),             #x legend (here should be SampleID) not shown
        strip.background = element_blank(),         #don't add background to the strips
        strip.placement = "outside",                #strip position ouside the plots
        strip.text.x = element_markdown(size = 18)) #read text as markdown
################################################################################
