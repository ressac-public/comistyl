# COMISTYL (COmmunauté Microbienne et Immunité de STYLirostris)

## The COMISTYL Project
COMISTYL is part of the scientific project of the LEAD research unit (Lagons, Ecosystèmes et Aquaculture Durable en Nouvelle-Calédonie). It is part of the framework agreement signed between Ifremer and the New Caledonian authorities in order to promote the sustainable development of aquaculture in New Caledonia

COMISTYL was developed to analyze the active microbiota of the shrimp's hemolymph and rearing water samples using metabarcoding approach. The aim of the project was to characterize the microbial communities inhabiting the hemolymphs of the Pacific blue shrimp Penaeus stylirostris at the individual level, according to rearing conditions and across two contrasted sites (New Caledonia and French Polynesia). It's a project lead by Nolwenn CALLAC from the UMR Entropie, LEAD-NC at Ifremer in New Caledonia.

## The COMISTYL Rmd file
The Rmd file was written to explain the logical steps of the analize of COMISTYL data in RStudio. It contains explicite information about installations, tools, functions and their use within the COMISTYL project.
To successfully run all scripts, you must download the source code of the project. 

## Project status
This project developed only to analyze the COMISTYL data as such, it is not meant to be maintained and evolve. Any use of the scripts must be part of an independant project and refer to this one under the License permission. 

## License
The project is under an MIT license and the copyright notice : 'Copyright (c) 2023 Ifremer' and this permission notice shall be included in all copies or substantial portions of the Software.
